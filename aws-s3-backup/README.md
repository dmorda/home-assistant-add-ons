# Home Assistant Add-on: Amazon S3 Backup

Backup your Home Assistant snapshots to an Amazon S3 bucket.

## About

The Amazon S3 Backup add-on for Home Assistant provides an easy way to
automatically backup your snapshots to an AWS S3 bucket. You can incorporate it
into an automation to perform routine backups of your configuration. This will
sync your Home Assistant `/backup/` directory to the s3 bucket.

[Read the full add-on documentation](DOCS.md)

## Authors & contributors

Damon Morda (dmorda@gmail.com)

## License

MIT License

Copyright 2020 Damon Morda

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
