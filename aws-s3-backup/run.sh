#!/bin/sh

KEY=`jq -r .awskey /data/options.json`
SECRET=`jq -r .awssecret /data/options.json`
BUCKET=`jq -r .bucketname /data/options.json`
HEARTBEATURL=`jq -r .heartbeaturl /data/options.json`

echo "Starting AWS S3 backup..."
echo "Configuring access key and secret..."

aws configure set aws_access_key_id $KEY
aws configure set aws_secret_access_key $SECRET

echo "Syncing /backup/ directory to S3..."

aws s3 sync /backup/ s3://$BUCKET/

echo "AWS S3 backup complete."

if [ -z "$HEARTBEATURL" ]
then
    echo "Heartbeat was not sent."
else
    wget --spider $HEARTBEATURL >/dev/null 2>&1
    echo "Heartbeat sent."
fi
