# Changelog
All notable changes to this project will be documented in this file.

## [1.0.0] - 2020-08-30
### Added
- Initial release
## [1.1.0] - 2021-01-23
### Added
- Support for heartbeat url
## [1.1.1] - 2024-06-06
- Addressed broken functionality after upgrading to Home Assistant 2024.6
